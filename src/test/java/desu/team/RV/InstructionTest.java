package desu.team.RV;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import desu.team.RV.Instruction.*;
import static desu.team.RV.Registers.*;
import static desu.team.RV.Opcodes.*;

public class InstructionTest {
	@Test
	void s0() {
		S s = S.parse(0xf6f72823); //f6f72823  sw  a5,-144(a4)
		assertEquals(A4, s.rs1); 
		assertEquals(A5, s.rs2);
		assertEquals(-144, s.imm);
	}
	@Test
	void s1() {
		S s = S.parse(0x00813423); //00813423  sd  s0,8(sp)
		assertEquals(SP, s.rs1); 
		assertEquals(S0, s.rs2);
		assertEquals(8, s.imm);
	}
	@Test
	void j0() {
		J j = J.parse(0x68c000ef); //8000000c: jal ra,80000698
		assertEquals(RA, j.rd, "Reg id");
		assertEquals(0x80000698 - 0x8000000c, j.imm, "Imm");
	}
	@Test
	void j1() {
		J j = J.parse(0x4cd030ef); //ffffffe000001068: jal ra,ffffffe000004d34
		assertEquals(RA, j.rd, "Reg id");
		assertEquals(0xffffffe000004d34L - 0xffffffe000001068L, j.imm, "Imm");
	}
	@Test
	void j2() {
		J j = J.parse(0xfcdfe0ef); //ffffffe000001074: jal ra,ffffffe000000040
		assertEquals(RA, j.rd, "Reg id");
		assertEquals(0xffffffe000000040L - 0xffffffe000001074L, j.imm, "Imm");
	}
	@Test
	void j3() {
		J j = J.parse(0x838ff0ef); //fffffffe000001078: jal ra,ffffffe0000000b0
		assertEquals(RA, j.rd, "Reg id");
		assertEquals(0xffffffe0000000b0L - 0xffffffe000001078L, j.imm, "Imm");
	}

	@Test
	void r0() {
		R r = R.parse(0x0118282f); //amoadd.w a6,a7,(a6)
		assertEquals(A6, r.rd, "Dest reg id");
		assertEquals(A6, r.rs1, "Src reg1 id");
		assertEquals(A7, r.rs2, "Src reg2 id");
		assertEquals(0b010, r.func3, "func3");
		assertEquals(AMO_W_ADD, r.amo_func, "func7");
	}
	@Test
	void r1() {
		R r = R.parse(0x40530e33); //sub  t3,t1,t0
		assertEquals(T3, r.rd, "Dest reg id");
		assertEquals(T1, r.rs1, "Src reg1 id");
		assertEquals(T0, r.rs2, "Src reg2 id");
		assertEquals(OP_SUB_F3, r.func3, "func3");
		assertEquals(OP_SUB_F7, r.func7, "func7");
	}

	@Test
	void r2() {
		R r = R.parse(0x0ef6b7af); //amoswap.d.aqrl  a5,a5,(a3)
		assertEquals(A5, r.rd, "Dest reg id");
		assertEquals(A3, r.rs1, "Src reg1 id");
		assertEquals(A5, r.rs2, "Src reg2 id");
		assertEquals(AMO_D, r.func3 , "func3");
		assertEquals(AMO_D_SWAP, r.amo_func, "func7");
	}
	@Test
	void u0() {
		U u = U.parse(0x00000817); //auipc   a6,0x0
		assertEquals(A6, u.rd, "Dest reg id");
		assertEquals(0, u.imm, "Imm");
	}
	@Test
	void u1() {
		U u = U.parse(0xd00e06b7); //lui   a3,0xd00e0
		assertEquals(A3, u.rd, "Dest reg id");
		assertEquals(0xd00e0 << 12, u.imm, "Imm");
	}
	@Test
	void i0() {
		I i = I.parse(0x50480813); //addi    a6,a6,1284
		assertEquals(A6, i.rd, "Dest reg id");
		assertEquals(A6, i.rs1, "Src reg id");
		assertEquals(1284, i.imm, "Imm");
	}
	@Test
	void i1() {
		I i = I.parse(0xfb010113); //addi    sp,sp,-80
		assertEquals(SP, i.rd, "Dest reg id");
		assertEquals(SP, i.rs1, "Src reg id");
		assertEquals(-80, i.imm, "Imm");
	}
	@Test
	void i2() {
		I i = I.parse(0x00813403); //ld      s0,8(sp)
		assertEquals(S0, i.rd, "Dest reg id");
		assertEquals(SP, i.rs1, "Src reg id");
		assertEquals(8, i.imm, "Imm");
	}
	@Test
	void i3() {
		I i = I.parse(0xffc7f793); //andi    a5,a5,-4
		assertEquals(A5, i.rd, "Dest reg id");
		assertEquals(A5, i.rs1, "Src reg id");
		assertEquals(-4, i.imm, "Imm");
	}
	
	
}
