package desu.team.RV;

import java.lang.reflect.Field;


public class CSR {


	public static final int FFLAGS 			= 0x001;
	public static final int FRM 			= 0x002;
	public static final int FCSR 			= 0x003;
	

	public static final int SSTATUS 		= 0x100;
	public static final int SEDELEG 		= 0x102;
	public static final int SIDELEG 		= 0x103;
	public static final int SIE 			= 0x104;
	public static final int STVEC 			= 0x105;
	public static final int SCOUNTEREN 		= 0x106;
	
	public static final int SATP 			= 0x180;

	public static final int SSCRATCH 		= 0x140;
	public static final int SEPC 			= 0x141;
	public static final int SCAUSE 			= 0x142;
	public static final int STVAL 			= 0x143;
	public static final int SIP 			= 0x144;
	
	public static final int MSTATUS 		= 0x300;
	public static final int MISA 			= 0x301;
	public static final int MEDELEG 		= 0x302;
	public static final int MIDELEG 		= 0x303;
	public static final int MIE 			= 0x304;
	public static final int MTVEC 			= 0x305;
	public static final int MCOUNTEREN 		= 0x306;
	
	public static final int PMCFG0	 		= 0x3a0;
	
	public static final int MSCRATCH 		= 0x340;
	public static final int MEPC 			= 0x341;
	public static final int MCAUSE 			= 0x342;
	public static final int MTVAL 			= 0x343;
	public static final int MIP				= 0x344;
	
	public static final int PMPADDR0 		= 0x3b0;
	public static final int PMPADDR1 		= 0x3b1;
	public static final int PMPADDR2 		= 0x3b2;
	public static final int PMPADDR3 		= 0x3b3;
	public static final int PMPADDR4 		= 0x3b4;
	public static final int PMPADDR5 		= 0x3b5;
	public static final int PMPADDR6 		= 0x3b6;
	public static final int PMPADDR7 		= 0x3b7;
	public static final int PMPADDR8 		= 0x3b8;
	public static final int PMPADDR9 		= 0x3b9;
	public static final int PMPADDR10 		= 0x3ba;
	public static final int PMPADDR11 		= 0x3bb;
	public static final int PMPADDR12 		= 0x3bc;
	public static final int PMPADDR13 		= 0x3bd;
	public static final int PMPADDR14 		= 0x3be;
	public static final int PMPADDR15 		= 0x3bf;
	public static final int MHARTID 		= 0xf14;

	public static final int MCYCLE 			= 0xb00;
	public static final int MINSRET		 	= 0xb02;
	public static final int MHPMCOUNTER3 	= 0xb03;
	public static final int MHPMCOUNTER4 	= 0xb04;
	public static final int MHPMCOUNTER5 	= 0xb05;
	public static final int MHPMCOUNTER6 	= 0xb06;
	public static final int MHPMCOUNTER7 	= 0xb07;
	public static final int MHPMCOUNTER8 	= 0xb08;
	public static final int MHPMCOUNTER9 	= 0xb09;
	public static final int MHPMCOUNTER10 	= 0xb0a;
	public static final int MHPMCOUNTER11 	= 0xb0b;
	public static final int MHPMCOUNTER12 	= 0xb0c;
	public static final int MHPMCOUNTER13 	= 0xb0d;
	public static final int MHPMCOUNTER14 	= 0xb0e;
	public static final int MHPMCOUNTER15 	= 0xb0f;
	public static final int MHPMCOUNTER16 	= 0xb10;
	public static final int MHPMCOUNTER17 	= 0xb11;
	public static final int MHPMCOUNTER18 	= 0xb12;
	public static final int MHPMCOUNTER19 	= 0xb13;
	public static final int MHPMCOUNTER20 	= 0xb14;
	public static final int MHPMCOUNTER21 	= 0xb15;
	public static final int MHPMCOUNTER22 	= 0xb16;
	public static final int MHPMCOUNTER23 	= 0xb17;
	public static final int MHPMCOUNTER24 	= 0xb18;
	public static final int MHPMCOUNTER25 	= 0xb19;
	public static final int MHPMCOUNTER26 	= 0xb1a;
	public static final int MHPMCOUNTER27 	= 0xb1b;
	public static final int MHPMCOUNTER28 	= 0xb1c;
	public static final int MHPMCOUNTER29 	= 0xb1d;
	public static final int MHPMCOUNTER30 	= 0xb1e;
	public static final int MHPMCOUNTER31 	= 0xb1f;
	
	
	//Bits
	public static final long MISA_I = 1 << 8;
	public static final long MISA_A = 1 << 0;
	public static final long MISA_M = 1 << 12;
	public static final long MISA_D = 1 << 3;
	public static final long MISA_F = 1 << 5;
	public static final long MISA_S = 1 << 18;
	public static final long MISA_U = 1 << 20;
	public static final long MISA_C = 1 << 2;
	public static final long MISA_MXL64 = 2L << 62;
	
	public static final long MSTATUS_MIE = 1L << 3;

	public static final int SATP_MODE_BARE 		= 0;
	public static final int SATP_MODE_SV39 		= 8;
	public static final int SATP_MODE_SV48 		= 9;
	

	long[] csrs = new long[0x1000];
	
	
	public long get(int index) {
		return csrs[index];
	}
	public void set(int index, long value) {
		csrs[index] = value;
	}
	
	public  String name(int index) {
		for(Field f : CSR.class.getDeclaredFields()) {
			try {
				if(f.getType() == int.class && f.getInt(null) == index)
					return f.getName().toLowerCase();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return String.format("UNKNOWN <%03x>", index);
	}
	
	public static boolean isReadOnly(int index) {
		int mask = Instruction.mask(2) << 10;
		return (index & mask) == mask;
	}
	public static int csrAcccessMinPrivLevel(int index) {
		int mask = Instruction.mask(2) << 8;
		return (index & mask) >> 8;
	}
	public boolean canRead(int index) {
		switch (index) {
			case SCOUNTEREN:
			case MCOUNTEREN:
				return false;
			default:
				return true;
		}
	}
	public boolean canWrite(int index) {
		switch (index) {
			case PMPADDR0:
				return false;
			default:
				return true;
		}
	}
}