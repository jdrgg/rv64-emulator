package desu.team.RV;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;

public class Loader {
	public static void load(String path, Memory memory, long base, int minSize) {
		try {
			
			DataInputStream is = new DataInputStream(new BufferedInputStream(new FileInputStream(path)));

			memory.allocateRam(base, Math.max(minSize, is.available()));
			System.out.printf("Loading kernel at address 0x%016x ", base);
			
			long cursor = base;
			for(;is.available() > 7;cursor += 8)
				memory.write64(cursor, Long.reverseBytes(is.readLong()));
			for(;is.available() > 0;cursor++)
				memory.write8(cursor, is.readByte());
			
			System.out.printf(", loaded 0x%08x bytes\n", cursor - base);
			
			is.close();
			
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
}
