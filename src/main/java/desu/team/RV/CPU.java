package desu.team.RV;

import static desu.team.RV.Opcodes.*;
import static desu.team.RV.Registers.*;

import static desu.team.RV.CSR.*;
import static desu.team.RV.Instruction.*;
import static desu.team.RV.Trap.*;
import static desu.team.RV.Logger.*;
import static desu.team.RV.Logger.Level.*;

public class CPU {

	long pc, nextPC;
	long[] registers;
	long[] fRegisters;
	
	Memory memory;
	SV49 mmu;
	CSR csr;
	StackTrace stackTrace;

	boolean slowEnabled = false;
	
	
	public CPU(Memory memory, SV49 mmu, CSR csr, long pc, long fdt) {
		this.pc = pc;
		this.memory = memory;
		this.mmu = mmu;
		this.csr = csr;
		registers = new long[32];
		fRegisters = new long[32];
		
		stackTrace = new StackTrace();
		
		
		registers[ZERO] = 0;
		registers[A0] = 0;//-1; //Hart id
		registers[A1] = fdt; //FDT pointer to device tree
		
		csr.csrs[MHARTID] = 0; //Hard id
		csr.csrs[MISA] = 
				MISA_I | 
				MISA_A | 
				MISA_M | 
				MISA_D | 
				MISA_F |
				MISA_S |
				MISA_MXL64;
	}

	
	private void setRegister(int index, long value) {
		if(index != 0)
			registers[index] = value;
	}
	
	
	public void step() {
		int instruction = memory.read32(mmu.translate(pc));

		if((instruction & 0b11) != 0b11) 
			throw new RuntimeException(String.format("%016x: C extension not supported", pc));
		
		nextPC = pc + 4;
		
		if(pc == 0x9000505cL)
			log(MARKER, "_start %016x \n", registers[S2]);

		
		i32(instruction);
		
		pc = nextPC;
		
	}
	

	
	private void i32(int instruction) {
		
		int opcode = instruction & mask(7);
		
		
		log(INSTRUCTION, "%08x ", instruction);
		
		U u;
		I i;
		J j;
		R r;
		B b;
		S s;
		
		
	
		if(
				((instruction & FENCE_ONES) == FENCE_ONES) &&
				((instruction | FENCE_ZEROS) == FENCE_ZEROS)
				) {
			log(INSTRUCTION, "FENCE\n");
			return;
		}
		if(instruction == FENCE_I) {
			log(INSTRUCTION, "FENCE.I\n");
			return;
		}
		if(instruction == MRET) {
			log(INSTRUCTION, "MRET\n");
			nextPC = csr.csrs[MEPC];
			return;
		}
		if(instruction == ECALL) {
			log(INSTRUCTION, "ECALL\n");
			exception(CAUSE_SUPERVISOR_ECALL, 0);
			return;
		}
		if(instruction == EBREAK) {
			log(INSTRUCTION, "EBREAK\n");
			throw new RuntimeException("EBREAK");
		}
		
		switch(opcode) {
			case JAL:
				j = J.parse(instruction);
				log(INSTRUCTION, "JAL %s,%x <%s>\n", registerName(j.rd), pc + j.imm, SymbolTable.name(pc + j.imm));
				
				if(j.rd == RA) {
					stackTrace.push(String.format("%016x: %s", pc, SymbolTable.name(pc)));
				}
				
				setRegister(j.rd, nextPC);
				nextPC = pc + j.imm;
				return;
				
			case SYSTEM:
				i = I.parse(instruction);
				switch(i.func3) {
					case SYSTEM_CSRRW: log(INSTRUCTION, "SYSTEM_CSRRW r1=%s\n", registerName(i.rs1));
						long reg = csr.get(i.offset);
						csr.set(i.offset, registers[i.rs1]);
						setRegister(i.rd, reg);
						return;

					case SYSTEM_CSRC: log(INSTRUCTION, "SYSTEM_CSRC\n");
						csr.set(i.offset, csr.get(i.offset) & (~registers[i.rs1]));
						return;

					case SYSTEM_CSRWI: log(INSTRUCTION, "SYSTEM_CSRWI\n");
						setRegister(i.rd, csr.get(i.offset));
						csr.set(i.offset, i.uimm);
						return;
						
					case SYSTEM_CSRRSI: log(INSTRUCTION, "SYSTEM_CSRRSI\n");
						long t = csr.get(i.offset);
						csr.set(i.offset, t | i.uimm);
						setRegister(i.rd, t);
						return;
					
					case SYSTEM_CSRRS: log(INSTRUCTION, "SYSTEM_CSRRS \n");
						long csrV = 0;
						if(!csr.canRead(i.offset)) {
							exception(CAUSE_ILLEGAL_INSTRUCTION, (long)instruction&0xFFFFFFFFL);
							return;
						}
						csrV = csr.get(i.offset);
						setRegister(i.rd, csrV);
						if(i.rs1 == ZERO)
							return;
						
						if(!csr.canWrite(i.offset)) {
							exception(CAUSE_ILLEGAL_INSTRUCTION, (long)instruction&0xFFFFFFFFL);
							return;
						}
						csr.set(i.offset, csrV | registers[i.rs1]);
						return;
						
					case SYSTEM_SFENCE_VMA: log(INSTRUCTION, "SYSTEM_SFENCE_VMA\n");
						return;
				}
				throw new RuntimeException(String.format("0x%08x: 0x%08x function not implemented, opcode SYSTEM",pc - 4,  i.func3));
			
			case AUIPC: 
				u = U.parse(instruction);
				log(INSTRUCTION, "AUIPC\n");
				setRegister(u.rd, pc+u.imm);
				return;
			
			case OP_IMM:
				i = I.parse(instruction);
				switch(i.func3) {
					case OP_IMM_ADDI: log(INSTRUCTION, "OP_IMM_ADDI\n");
						setRegister(i.rd, registers[i.rs1] + i.imm);
						return;
					case OP_IMM_XORI: log(INSTRUCTION, "OP_IMM_XORI\n");
						setRegister(i.rd, registers[i.rs1] ^ ((i.imm << 20) >> 20));
						return;
					case OP_IMM_SLLI: log(INSTRUCTION, "OP_IMM_SLLI\n");
						setRegister(i.rd, registers[i.rs1] << i.shamt);
						return;
					case OP_IMM_SRLI: log(INSTRUCTION, "OP_IMM_SRLI\n");
						setRegister(i.rd, registers[i.rs1] >>> i.shamt);
						return;
					case OP_IMM_ANDI: log(INSTRUCTION, "OP_IMM_ANDI\n");
						setRegister(i.rd, registers[i.rs1] & (long)i.imm);
						return;
					case OP_IMM_ORI: log(INSTRUCTION, "OP_IMM_ORI\n");
						setRegister(i.rd, registers[i.rs1] | (long)i.imm);
						return;
					case OP_IMM_SLTIU: log(INSTRUCTION, "OP_IMM_SLTIU\n");
						setRegister(i.rd, Long.compareUnsigned(registers[i.rs1], (long)i.imm) < 0 ? 1 : 0);
						return;
					case OP_IMM_SLTI: log(INSTRUCTION, "OP_IMM_SLTI\n");
						setRegister(i.rd, registers[i.rs1] < (long)i.imm ? 1 : 0);
						return;
				}
			throw new RuntimeException(String.format("0x%08x: 0x%08x function not implemented, opcode OP_IMM",pc,  i.func3));
			
			case LUI:
				u = U.parse(instruction);
				log(INSTRUCTION, "LUI %d\n", u.imm);
				setRegister(u.rd, (u.imm<<32)>>>32);
				return;
			
			case AMO:
				r = R.parse(instruction);
				switch(r.func3) {
					case AMO_W:
						switch (r.amo_func) {
						case AMO_W_ADD: log(INSTRUCTION, "AMO_W_ADD\n");
							long rdValue = (long)memory.read32(mmu.translate(registers[r.rs1])) & 0xFFFFFFFFL;
							memory.write32(mmu.translate(registers[r.rs1]), (int)(rdValue + registers[r.rs2]));
							setRegister(r.rd, rdValue);
							return;

						case AMO_W_SWAP: log(INSTRUCTION, "AMO_W_SWAP\n");
							rdValue = (long)memory.read32(mmu.translate(registers[r.rs1])) & 0xFFFFFFFFL;
							memory.write32(mmu.translate(registers[r.rs1]), (int)(registers[r.rs2] & 0xFFFFFFFFL));
							setRegister(r.rd, rdValue);
							return;
						}
						throw new RuntimeException(String.format("0x%08x: 0x%08x function not implemented, opcode AMO_W",pc,  r.func7));
					case AMO_D:
						switch (r.amo_func) {
						case AMO_D_SWAP: log(INSTRUCTION, "AMO_D_SWAP\n");
							long rdValue = memory.read64(mmu.translate(registers[r.rs1]));
							memory.write64(mmu.translate(registers[r.rs1]), registers[r.rs2]);
							setRegister(r.rd, rdValue);
							return;
						case AMO_D_LR: log(INSTRUCTION, "AMO_D_LR\n");
							setRegister(r.rd, memory.read64(mmu.translate(registers[r.rs1])));
							return;
						case AMO_D_SC: log(INSTRUCTION, "AMO_D_SC\n");
							memory.write64(mmu.translate(registers[r.rs1]), registers[r.rs2]);
							setRegister(r.rd, 0/* OK */);
							return;
					}
						throw new RuntimeException(String.format("0x%08x: 0x%08x function not implemented, opcode AMO_D",pc,  r.amo_func));
						
					
				}
				return;
			case BR:
				b = B.parse(instruction);
				switch(b.func3) {
					case BR_NE: log(INSTRUCTION, "BR_NE %016x %016x \n", registers[b.rs1], registers[b.rs2]);
						if(registers[b.rs1] != registers[b.rs2])
							nextPC = pc + b.imm;
						return;
					case BR_EQ: log(INSTRUCTION, "BR_EQ 0x%016x = 0x%016x \n", registers[b.rs1], registers[b.rs2]);
						if(registers[b.rs1] == registers[b.rs2])
							nextPC = pc + b.imm;
						return;
					case BR_LT: log(INSTRUCTION, "BR_LT\n");
						if(registers[b.rs1] < registers[b.rs2])
							nextPC = pc + b.imm;
						return;
					case BR_GE: log(INSTRUCTION, "BR_GE\n");
						if(registers[b.rs1] >= registers[b.rs2])
							nextPC = pc + b.imm;
						return;
					case BR_GEU: log(INSTRUCTION, "BR_GEU\n");
						if(Long.compareUnsigned(registers[b.rs1], registers[b.rs2]) != -1)
							nextPC = pc + b.imm;
						return;
					case BR_LTU: log(INSTRUCTION, "BR_LTU\n");
						if(Long.compareUnsigned(registers[b.rs1], registers[b.rs2]) == -1)
							nextPC = pc + b.imm;
						return;
				}
				throw new RuntimeException(String.format("0x%08x: 0x%08x function not implemented, opcode BR",pc,  b.func3));
			
			case STORE:
				s = S.parse(instruction);
				switch(s.func3) {
					case STORE_SD: log(INSTRUCTION, "STORE_SD\n");
						memory.write64(mmu.translate(registers[s.rs1]+s.imm), registers[s.rs2]);
						return;

					case STORE_SW: log(INSTRUCTION, "STORE_SW\n");
						memory.write32(mmu.translate(registers[s.rs1]+s.imm), (int)registers[s.rs2]);
						return;

					case STORE_SB: log(INSTRUCTION, "STORE_SB\n");
						memory.write8(mmu.translate(registers[s.rs1]+s.imm), (byte)registers[s.rs2]);
						return;
					case STORE_SH: log(INSTRUCTION, "STORE_SB\n");
						memory.write16(mmu.translate(registers[s.rs1]+s.imm), (short)registers[s.rs2]);
						return;
				}
				throw new RuntimeException(String.format("0x%08x: 0x%1x function not implemented, opcode STORE",pc,  s.func3));
				
			case LOAD: 
				i = I.parse(instruction);
				switch (i.func3) {
					case LOAD_LD:
						log(INSTRUCTION, "LD [%s] = 0x%016x\n", registerName(i.rd), memory.read64(registers[i.rs1]+i.imm));
						setRegister(i.rd, memory.read64(mmu.translate(registers[i.rs1]+i.imm)));
						return;
					case LOAD_LBU:
						log(INSTRUCTION, "LBU [%s] = 0x%02x\n", registerName(i.rd), memory.read8(registers[i.rs1]+i.imm));
						setRegister(i.rd, (long)memory.read8(mmu.translate(registers[i.rs1]+i.imm)) & 0xFF);
						return;
					case LOAD_LW:
						log(INSTRUCTION, "LW [%s] = 0x%08x\n", registerName(i.rd), memory.read32(registers[i.rs1]+i.imm));
						setRegister(i.rd, (long)memory.read32(mmu.translate(registers[i.rs1]+i.imm)));
						return;
					case LOAD_LWU:
						log(INSTRUCTION, "LWU [%s] = 0x%08x\n", registerName(i.rd), memory.read32(registers[i.rs1]+i.imm));
						setRegister(i.rd, ((long)memory.read32(mmu.translate(registers[i.rs1]+i.imm))) & 0xFFFFFFFFL);
						return;
					case LOAD_LHU:
						log(INSTRUCTION, "LWU [%s] = 0x%04x\n", registerName(i.rd), memory.read16(registers[i.rs1]+i.imm));
						setRegister(i.rd, ((long)memory.read16(mmu.translate(registers[i.rs1]+i.imm))) & 0xFFFFL);
						return;
					
				}

				throw new RuntimeException(String.format("0x%08x: 0x%1x function not implemented, opcode LOAD", pc,  i.func3));
			
			case JALR:
				i = I.parse(instruction);
				log(INSTRUCTION, "JALR [AO]: %016x\n", registers[A0]);
				
				if(i.rd == ZERO && i.rs1 == RA && i.imm == 0) { // ret
					log(LOCATION, "RET [AO]: %016x\n", registers[A0]);
					stackTrace.pop();
				}
						
				long lt = registers[i.rs1];
				setRegister(i.rd, nextPC);
				nextPC = lt+i.imm;
				return;
				
			case OP:
				r = R.parse(instruction);
				
				if(r.func3 == OP_ADD_F3 && r.func7 == OP_ADD_F7){
					log(INSTRUCTION, "OP_ADD\n");
					setRegister(r.rd, registers[r.rs1] + registers[r.rs2]);
					return;
				}
				if(r.func3 == OP_SUB_F3 && r.func7 == OP_SUB_F7){
					log(INSTRUCTION, "OP_SUB\n");
					setRegister(r.rd, registers[r.rs1] - registers[r.rs2]);
					return;
				}
				if(r.func3 == OP_OR_F3 && r.func7 == OP_OR_F7){
					log(INSTRUCTION, "OP_OR\n");
					setRegister(r.rd, registers[r.rs1] | registers[r.rs2]);
					return;
				}
				if(r.func3 == OP_MUL_F3 && r.func7 == OP_MUL_F7){
					log(INSTRUCTION, "OP_MUL\n");
					setRegister(r.rd, registers[r.rs1] * registers[r.rs2]);
					return;
				}
				if(r.func3 == OP_AND_F3 && r.func7 == OP_AND_F7){
					log(INSTRUCTION, "OP_AND\n");
					setRegister(r.rd, registers[r.rs1] & registers[r.rs2]);
					return;
				}
				if(r.func3 == OP_SLL_F3 && r.func7 == OP_SLL_F7){
					log(INSTRUCTION, "OP_SLL\n");
					setRegister(r.rd, registers[r.rs1] << (registers[r.rs2] & mask(5)));
					return;
				}
				if(r.func3 == OP_SRL_F3 && r.func7 == OP_SRL_F7){
					log(INSTRUCTION, "OP_SRL\n");
					setRegister(r.rd, registers[r.rs1] >>> (registers[r.rs2] & mask(5)));
					return;
				}
				if(r.func3 == OP_SLTU_F3 && r.func7 == OP_SLTU_F7){
					log(INSTRUCTION, "OP_SLTU\n");
					setRegister(r.rd, Long.compareUnsigned(registers[r.rs1], registers[r.rs2]) < 0 ?1:0);
					return;
				}
				if(r.func3 == OP_XOR_F3 && r.func7 == OP_XOR_F7){
					log(INSTRUCTION, "OP_XOR\n");
					setRegister(r.rd, registers[r.rs1] ^ registers[r.rs2]);
					return;
				}
				if(r.func3 == OP_REMU_F3 && r.func7 == OP_REMU_F7){
					log(INSTRUCTION, "OP_REMU\n");
					setRegister(r.rd, Long.remainderUnsigned(registers[r.rs1], registers[r.rs2]));
					return;
				}
				if(r.func3 == OP_DIVU_F3 && r.func7 == OP_DIVU_F7){
					log(INSTRUCTION, "OP_DIVU\n");
					setRegister(r.rd, Long.divideUnsigned(registers[r.rs1], registers[r.rs2]));
					return;
				}
				
				throw new RuntimeException(String.format("0x%08x: 0x%1x 0x%02x function not implemented, opcode OP",pc,  r.func3, r.func7));
			case OPW:
				r = R.parse(instruction);
				
				if(r.func3 == OPW_ADDW_F3 && r.func7 == OPW_ADDW_F7){
					log(INSTRUCTION, "OPW_ADDW\n");
					setRegister(r.rd, (long)(int)(registers[r.rs1]+registers[r.rs2]) & 0xFFFFFFFFL);
					return;
				}
				if(r.func3 == OPW_SUBW_F3 && r.func7 == OPW_SUBW_F7){
					log(INSTRUCTION, "OPW_SUBW\n");
					setRegister(r.rd, (long)(int)(registers[r.rs1]-registers[r.rs2]) & 0xFFFFFFFFL);
					return;
				}
				if(r.func3 == OPW_SLLW_F3 && r.func7 == OPW_SLLW_F7){
					log(INSTRUCTION, "OPW_SLLW\n");
					setRegister(r.rd, (long)(int)(registers[r.rs1]<<(registers[r.rs2]&mask(5))) & 0xFFFFFFFFL);
					return;
				}
				
				throw new RuntimeException(String.format("0x%08x: 0x%1x 0x%02x function not implemented, opcode OPW",pc,  r.func3, r.func7));
				
			case SEXT:
				i = I.parse(instruction);
				switch (i.func3) {
					case SEXT_ADDIW:log(INSTRUCTION, "SEXT_ADDIW\n");
						int t = (int)registers[i.rs1];
						t += (i.imm<<20)>>20;
						setRegister(i.rd, t);
						return;
					case SEXT_SLLIW:
						t = (int)registers[i.rs1];
						int t2 = t << (i.imm & mask(5));
						log(INSTRUCTION, "SEXT_SLLIW\n");
						setRegister(i.rd, t2);
						return;
					case SEXT_SRLIW:
						t = (int)registers[i.rs1];
						t >>= (i.imm & mask(5));
						log(INSTRUCTION, "SEXT_SRLIW\n");
						setRegister(i.rd, t);
						return;
				}
				throw new RuntimeException(String.format("0x%08x: 0x%1x function not implemented, opcode SEXT",pc,  i.func3));
				
			case FMOV:
				r = R.parse(instruction);
				switch(r.func7) {
				case FMOV_FMV_X_D: log(INSTRUCTION, "FMOV_FMV_X_D\n");
					if(r.rs2 != 0) throw new RuntimeException();
					if(r.rm != 0) throw new RuntimeException();
					setRegister(r.rd, fRegisters[r.rs1]);
					return;
				case FMOV_FMV_W_X: log(INSTRUCTION, "FMOV_FMV_X_D\n");
					if(r.rs2 != 0) throw new RuntimeException();
					if(r.rm != 0) throw new RuntimeException();
					fRegisters[r.rd] = registers[r.rs1] & 0xFFFFFFFFL;
					return;
				}
				throw new RuntimeException(String.format("0x%08x: 0x%2x function not implemented, opcode FMOV",pc,  r.func7));
				
		}
		
		
		throw new RuntimeException(String.format("0x%08x: 0x%08x instruction not implemented, opcode %s",pc,  instruction, Integer.toBinaryString(opcode)));
		
	}
	
	private void exception(long cause, long val) {
		// check if M mode interrupts disabled
		//if((csrs[MSTATUS] & MSTATUS_MIE) == 0)
		//	return;
		
		log(MARKER, "TRAP\n");
		csr.csrs[MEPC] = pc;
		csr.csrs[MCAUSE] = cause;
		csr.csrs[MTVAL] = val;
		nextPC = csr.csrs[MTVEC];
	}
	
	
}
