package desu.team.RV;

import static desu.team.RV.Instruction.mask;
import static desu.team.RV.Instruction.lmask;

public class SV49 {
	private Memory memory;
	private CSR csr;
	
	public SV49(Memory memory, CSR csr) {
		this.memory = memory;
		this.csr = csr;
	}
	
	public long translate(long virtAddr) {
		int mode = getMode();
		if(mode == CSR.SATP_MODE_BARE)
			return virtAddr;
		if(mode != CSR.SATP_MODE_SV39)
			throw new RuntimeException("Unsupported mode: "+mode);
		
		long rootPageAddr = getRootPage() << 11;
		
		long offset = virtAddr & mask(11);
		long[] vpn = new long[] {
				(virtAddr >> 12) & mask(9),
				(virtAddr >> 21) & mask(9),
				(virtAddr >> 30) & mask(9)
		};
		long rest = (virtAddr >> 39);
		
		if(rest != 0 && rest != -1) {
			throw new RuntimeException("page fault vaddr:"+Long.toHexString(virtAddr));
		}

		
		
		long addr = rootPageAddr;
		
		for(int i = 0;i < 3;i++) {
			long entry = memory.read64((addr << 11) | vpn[i]);
			
			addr = (entry >> 10) & lmask(44);
			if(entry % 8 == 0)
				break;
		}
		
		long paddress = (addr << 11) | offset;
		
		System.out.println(virtAddr +" -> "+ paddress);
		
		return paddress;
	}
	
	
	
	private long getRootPage() {
		return csr.csrs[CSR.SATP] & lmask(44);
	}
	private int getMode() {
		return (int)((csr.csrs[CSR.SATP] >> 60) & mask(4));
	}
}
