package desu.team.RV;

import java.util.Stack;

public class StackTrace extends Stack<String> {
	
	private static final long serialVersionUID = 1L;
	public static boolean enable = true;
	
	

	public void print() {
		System.out.println("Stack:\n");
		for(String frame : this) {
			System.out.println("\t\t" + frame);
		}
	}
	
	@Override
	public synchronized String pop() {
		if(StackTrace.enable && size() > 0)
			return super.pop();
		return "";
	}
	@Override
	public String push(String item) {
		if(StackTrace.enable)
			return super.push(item);
		return "";
	}
}
