package desu.team.RV;

public class App {
    public static void main(String[] args) throws Exception {
    	new App();
    }
    
    public App() throws Exception {
    	
    	Memory memory = new Memory();

    	memory.regions.add(new Memory.SifiveUart(0x70000000L));
    	memory.regions.add(new Memory.RamRegion(0xffffffe000000000L, 128*1024*1024));
    	memory.regions.add(new Memory.RamRegion(0x0000000000200000L, 128*1024*1024));

    	long sbiBase 		= 0x080000000L;
    	long fdtBase 		= 0x100000000L;
    	long payloadBase 	= 0x090000000L;
    	
    	//Load Linux
    	SymbolTable.load("/home/jdr/git/linux-5.9.1/vmlinux", 0xffffffe000000000L, payloadBase);
    	//Kernel.load("/home/jdr/git/opensbi/build/platform/generic/firmware/payloads/test.bin", memory, payloadBase, 128*1024*1024);
    	Loader.load("/home/jdr/git/linux-5.9.1/arch/riscv/boot/Image", memory, payloadBase, 128*1024*1024);
    	//Load OpenSBI
    	SymbolTable.load("/home/jdr/git/opensbi/build/platform/generic/firmware/fw_jump.elf", 0x080000000L, sbiBase);
    	Loader.load("/home/jdr/git/opensbi/build/platform/generic/firmware/fw_jump.bin", memory, sbiBase, 128*0x200000);
    	//Load Device tree
    	Loader.load("res/tree.dtb", memory, fdtBase, 0);
    	//Kernel.load("/home/jdr/git/opensbi/riscv64-virt.dtb", memory, fdtBase, 0);
    	
    	CSR csr = new CSR();
    	SV49 mmu = new SV49(memory, csr);
    	CPU cpu = new CPU(memory, mmu, csr, sbiBase, fdtBase);
    	Logger.cpu = cpu;
    	
    	while(true) {
    		cpu.step();
    		System.out.flush();
    		System.err.flush();
    		
    		if(cpu.slowEnabled) {
    			Thread.sleep(1000);
    		}
    	}
    	
    	
	}
}
