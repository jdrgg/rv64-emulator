package desu.team.RV;

public class Logger {
	public static enum Level {
		MARKER,
		CSR,
		INSTRUCTION,
		CONSOLE,
		LOCATION
	}
	
	public static boolean logEnabled = !true;
	public static boolean newLine = true;
	public static CPU cpu;
	
	
	
	public static void log(Level level, String pattern, Object ...params) {
		if(!logEnabled)
			return;
		if(true || level == Level.CSR) {
			if(newLine) {
				System.out.printf("%016x: ", Logger.cpu.pc - 4);
				//System.out.printf("%s: ", SymbolTable.name(pc - 4));
			}
			System.out.printf(pattern, params);
			newLine = pattern.endsWith("\n");
		}
	}
}
