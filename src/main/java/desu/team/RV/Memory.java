package desu.team.RV;

import java.util.ArrayList;
import java.util.List;

public class Memory {
	
	List<Region> regions = new ArrayList<>();
	
	public void allocateRam(long base, int size) {
		//Align to 8 bytes
		if((size & 0b111) != 0) {
			size += 8;
			size &= ~0b111;
		}
		
		System.out.printf("Memory: New ram region base=0x%016x size=0x%08x\n", base, size);
		addRegion(new RamRegion(base, size));
	}
	
	
	public void addRegion(Region region) {
		
		for(Region r : regions) {
			if(r.overlap(region))
				throw new RuntimeException("New region overlap with old one");
		}
		
		regions.add(region);
	}
	public Region region(long address) {
		for (Region region : regions) {
			if(region.contains(address))
				return region;
		}
		throw new RuntimeException(String.format("Memory: Can't find region for address: 0x%016x", address));
	}

	public void write8(long address, byte value) {
		region(address).write8(address, value);
	}

	public short read16(long address) {
		return region(address).read16(address);
	}
	public void write16(long address, short value) {
		region(address).write16(address, value);
	}
	public void write32(long address, int value) {
		region(address).write32(address, value);
	}
	public void write64(long address, long value) {
		region(address).write64(address, value);
	}
	public byte read8(long address) {
		return region(address).read8(address);
	}
	public int read32(long address) {
		return region(address).read32(address);
	}
	public long read64(long address) {
		return region(address).read64(address);
	}

	public String readCString(long address) {
		return readCString(address, 0xfffffff);
	}

	public String readCString(long address, int maxLength) {
		StringBuffer sb = new StringBuffer();
		byte c;
		while((c = read8(address)) != 0 && maxLength >= 0) {
			sb.append((char)((int)c&0xFF));
			address++;
			maxLength--;
		}
		return sb.toString();
	}

	
	static abstract class Region {
		long base, end, size;
		public Region(long base, long size) {
			this.base = base;
			this.size = size;
			end = base + size;
		}
		
		boolean overlap(Region region) {
			return Math.max(base, region.base) < Math.min(end, region.end);
		}
		boolean contains(long address) {
			return address >= base && address < end;
		}
		short read16(long address) {
			if(address % 2 != 0)
				throw new RuntimeException(String.format("Address %08x not aligned", address));
			return (short)(
					((short)read8(address+1) & 0xFF) <<  8 |
					((short)read8(address+0) & 0xFF) <<  0);
		}
		void write16(long address, short value) {
			if(address % 2 != 0)
				throw new RuntimeException(String.format("Address %08x not aligned", address));
			write8(address+1, (byte)((value >>  8)&0xFF));
			write8(address+0, (byte)((value >> 00)&0xFF));
			
		}
		int read32(long address) {
			if(address % 4 != 0)
				throw new RuntimeException(String.format("Address %08x not aligned", address));
			return
					((int)read8(address+3) & 0xFF) << 24 |
					((int)read8(address+2) & 0xFF) << 16 |
					((int)read8(address+1) & 0xFF) <<  8 |
					((int)read8(address+0) & 0xFF) <<  0;
		}
		void write32(long address, int value) {
			if(address % 4 != 0)
				throw new RuntimeException(String.format("Address %08x not aligned", address));
			write8(address+3, (byte)((value >> 24)&0xFF));
			write8(address+2, (byte)((value >> 16)&0xFF));
			write8(address+1, (byte)((value >>  8)&0xFF));
			write8(address+0, (byte)((value >> 00)&0xFF));
			
		}
		long read64(long address) {
			if(address % 4 != 0)
				throw new RuntimeException(String.format("Address %08x not aligned", address));
			return
					((long)read8(address+7) & 0xFF) << 56 |
					((long)read8(address+6) & 0xFF) << 48 |
					((long)read8(address+5) & 0xFF) << 40 |
					((long)read8(address+4) & 0xFF) << 32 |
					((long)read8(address+3) & 0xFF) << 24 |
					((long)read8(address+2) & 0xFF) << 16 |
					((long)read8(address+1) & 0xFF) <<  8 |
					((long)read8(address+0) & 0xFF) <<  0;
		}
		void write64(long address, long value) {
			if(address % 4 != 0)
				throw new RuntimeException(String.format("Address %08x not aligned", address));

			write8(address+7, (byte)((value >> 56)&0xFF));
			write8(address+6, (byte)((value >> 48)&0xFF));
			write8(address+5, (byte)((value >> 40)&0xFF));
			write8(address+4, (byte)((value >> 32)&0xFF));
			write8(address+3, (byte)((value >> 24)&0xFF));
			write8(address+2, (byte)((value >> 16)&0xFF));
			write8(address+1, (byte)((value >>  8)&0xFF));
			write8(address+0, (byte)((value >>  0)&0xFF));
			
		}
		
		
		abstract byte read8(long address);
		abstract void write8(long address, byte value);
	}
	
	static class RamRegion extends Region {
		byte[] buffer;
		
		public RamRegion(long base, int size) {
			super(base, size);
			buffer = new byte[size];
		}
		@Override
		byte read8(long address) {
			return buffer[(int)(address - base)];
		}

		@Override
		void write8(long address, byte value) {
			buffer[(int)(address - base)] = value;
		}
	}

	public static class SifiveUart extends Region {
		public static final int REG_TXFIFO	= 0;
		public static final int REG_RXFIFO	= 1;
		public static final int REG_TXCTRL	= 2;
		public static final int REG_RXCTRL	= 3;
		public static final int REG_IE		= 4;
		public static final int REG_IP		= 5;
		public static final int REG_DIV		= 6;

		public static final int TXFIFO_FULL	= 0x80000000;
		public static final int RXFIFO_EMPTY	= 0x80000000;
		public static final int RXFIFO_DATA	= 0x000000ff;
		public static final int TXCTRL_TXEN	= 0x1;
		public static final int RXCTRL_RXEN	= 0x1;

		public SifiveUart(long base) {
			super(base, 7*4);
		}

		@Override
		int read32(long address) {
			switch((int)(address - base)/4)	{
				case REG_TXFIFO:
					return ~TXFIFO_FULL;
			}
			throw new RuntimeException();
		}

		@Override
		void write32(long address, int value) {
			switch((int)(address - base)/4)	{
				case REG_IE:
				case REG_IP:
				case REG_DIV:
				case REG_RXCTRL:
				case REG_TXCTRL:
					//Ignored
					return;
			
				case REG_TXFIFO:
					System.out.printf("%c", (char)(value&0xFF));
					return;
			}
			throw new RuntimeException(""+(address - base));
		}

		@Override
		byte read8(long address) {
			throw new RuntimeException();
		}
		@Override
		void write8(long address, byte value) {
			throw new RuntimeException();
		}
	}
	
	
}
