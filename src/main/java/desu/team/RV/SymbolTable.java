package desu.team.RV;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SymbolTable {
	
	static List<Symbol> table = new ArrayList<>();
	
	static void load(String elf, long elfBase, long memoryBase) throws Exception {
		ProcessBuilder pb = new ProcessBuilder("readelf", "-sW", elf);
		Process p = pb.start();
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		
		Pattern pattern = Pattern.compile("\\ +(\\d+):\\ +([0-9a-f]+)\\ +(\\d+)\\ +(\\w+)\\ +(\\w+)\\ +(\\w+)\\ +(\\w+)\\ ([\\w\\.\\/]+)");
		
		String line;
		
		while((line = br.readLine())!= null) {
			Matcher matcher = pattern.matcher(line);
			if(!matcher.matches())
				continue;
			String base = matcher.group(2);
			String size = matcher.group(3);
			String type = matcher.group(4);
			String name = matcher.group(8);
			
			
			
			BigInteger addr = (new BigInteger(base, 16)).subtract(toUnsignedBigInteger(elfBase)).add(toUnsignedBigInteger(memoryBase));
			
			if(addr.compareTo(toUnsignedBigInteger(elfBase)) >= 0) {
				table.add(new Symbol(addr.longValueExact(), Integer.parseInt(size), name, type));
			}
			
		}
		br.close();
		p.waitFor();
		
	}
	static String nameExact(long address) {
		for (Symbol symbol : table) {
			if(symbol.base == address)
				return symbol.name;
		}
		return "UNKNOWN";
	}
	static String name(long address) {
		for (Symbol symbol : table) {
			if(symbol.base == address)
				return symbol.name;
		}
		for (Symbol symbol : table) {
			if(symbol.contains(address))
				return String.format("%s + %x", symbol.name, address - symbol.base);
		}
		return "UNKNOWN";
	}
	
	private static BigInteger toUnsignedBigInteger(long i) {
	    if (i >= 0L)
	        return BigInteger.valueOf(i);
	    else {
	        int upper = (int) (i >>> 32);
	        int lower = (int) i;

	        // return (upper << 32) + lower
	        return (BigInteger.valueOf(Integer.toUnsignedLong(upper))).shiftLeft(32).
	            add(BigInteger.valueOf(Integer.toUnsignedLong(lower)));
	    }
	}
	
	
	
	
	static class Symbol {
		long base;
		int size;
		String name, type;
		
		public Symbol(long base, int size, String name, String type) {
			this.base = base;
			this.size = size;
			this.name = name;
			this.type = type;
		}
		
		boolean contains(long address) {
			return address >= base && address < (base + size);
		}
	}
}
