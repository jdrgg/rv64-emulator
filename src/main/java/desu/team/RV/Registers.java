package desu.team.RV;

import java.lang.reflect.Field;

public class Registers {
	public static final int ZERO = 0;	// Hard-wired zero					---
	public static final int RA = 1;		// Return address					Caller
	public static final int SP = 2;		// Stack pointer					Callee
	public static final int GP = 3;		// Global pointer					---
	public static final int TP = 4;		// Thread pointer					---
	public static final int T0 = 5;		// Temporary						Caller
	public static final int T1 = 6;		// Temporary						Caller
	public static final int T2 = 7;		// Temporary						Caller
	public static final int S0 = 8;		// Saved register					Callee
	public static final int S1 = 9;		// Saved register					Callee
	public static final int A0 = 10;	// Function argument/return value	Caller
	public static final int A1 = 11;	// Function argument/return value	Caller
	public static final int A2 = 12;	// Function argument				Caller
	public static final int A3 = 13;	// Function argument				Caller
	public static final int A4 = 14;	// Function argument				Caller
	public static final int A5 = 15;	// Function argument				Caller
	public static final int A6 = 16;	// Function argument				Caller
	public static final int A7 = 17;	// Function argument				Caller
	public static final int S2 = 18;	// Saved register					Callee
	public static final int S3 = 19;	// Saved register					Callee
	public static final int S4 = 20;	// Saved register					Callee
	public static final int S5 = 21;	// Saved register					Callee
	public static final int S6 = 22;	// Saved register					Callee
	public static final int S7 = 23;	// Saved register					Callee
	public static final int S8 = 24;	// Saved register					Callee
	public static final int S9 = 25;	// Saved register					Callee
	public static final int S10 = 26;	// Saved register					Callee
	public static final int S11 = 27;	// Saved register					Callee
	public static final int T3 = 28;	// Temporary						Caller
	public static final int T4 = 29;	// Temporary						Caller
	public static final int T5 = 30;	// Temporary						Caller
	public static final int T6 = 31;	// Temporary						Caller
	
	//F extension
	public static final int FT0 = 0;
	public static final int FT1 = 1;
	public static final int FT2 = 2;
	public static final int FT3 = 3;
	public static final int FT4 = 4;
	public static final int FT5 = 5;
	public static final int FT6 = 6;
	public static final int FT7 = 7;
	public static final int FS0 = 8;
	public static final int FS1 = 9;
	public static final int FA0 = 10;
	public static final int FA1 = 11;
	public static final int FA2 = 12;
	public static final int FA3 = 13;
	public static final int FA4 = 14;
	public static final int FA5 = 15;
	public static final int FA6 = 16;
	public static final int FA7 = 17;
	public static final int FS2 = 18;
	public static final int FS3 = 19;
	public static final int FS4 = 20;
	public static final int FS5 = 21;
	public static final int FS6 = 22;
	public static final int FS7 = 23;
	public static final int FS8 = 24;
	public static final int FS9 = 25;
	public static final int FS10 = 26;
	public static final int FS11 = 27;
	public static final int FT8 = 28;
	public static final int FT9 = 29;
	public static final int FT10 = 30;
	public static final int FT11 = 31;
	
	
	public static String registerName(int index) {
		for(Field f : Registers.class.getDeclaredFields()) {
			try {
				if(f.getType() == int.class && !f.getName().startsWith("F")&& f.getInt(null) == index)
					return f.getName().toLowerCase();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		throw new RuntimeException(String.format("UNKNOWN <%02x>", index));
	}
	public static String registerNameF(int index) {
		for(Field f : Registers.class.getDeclaredFields()) {
			try {
				if(f.getType() == int.class && f.getName().startsWith("F") && f.getInt(null) == index)
					return f.getName().toLowerCase();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		throw new RuntimeException(String.format("UNKNOWN F <%02x>", index));
	}
}
