package desu.team.RV;

public class Instruction {
	public static int mask(int n) {
		if(n > 31) throw new RuntimeException("use lmask");
		return ((1 << n)-1);
	}
	public static long lmask(int n) {
		return ((1L << n)-1L);
	}
	public static class J {
		int rd;
		int imm;
		public J(int rd, int imm) {
			this.rd = rd;
			this.imm = imm;
		}
		public static J parse(int data) {
			int imm = data & (mask(1) << 31);
			imm |= (data & (mask(8) << 12));
			imm |= (data & (mask(1) << 20)) >> 9;
			imm |= (data & (mask(10) << 21)) >> 20;

			imm <<= 12;
			imm >>= 12;
			return new J((data >> 7) & mask(5), imm);
		}
	}
	
	public static class I {
		int rd;
		int func3;
		int rs1;
		int imm;
		
		int uimm;
		int offset;
		int shamt;
		
		public I(int rd, int func3, int rs1, int imm) {
			this.rd = rd;
			this.func3 = func3;
			this.rs1 = rs1;
			this.imm = imm;
			
			uimm = rs1;
			offset = imm & mask(12);
			shamt = imm & mask(6);
		}
		public static I parse(int data) {
			return new I(
					(data >>  7) & mask(5),
					(data >> 12) & mask(3),
					(data >> 15) & mask(5),
					(data >> 20)
			);
		}
	}
	public static class U {
		int rd;
		int imm;
		public U(int rd, int imm) {
			this.rd = rd;
			this.imm = imm;
		}
		public static U parse(int data) {
			return new U(
					(data >> 7) & mask(5),
					data & (mask(20) << 12)
			);
		}
	}
	public static class R {
		int rd;
		int func3;
		int rs1;
		int rs2;
		int func7;
		int amo_func;
		int rm; //Rownding mode float point
		
		public R(int rd, int func3, int rs1, int rs2, int func7, int amo_func) {
			this.rd = rd;
			this.func3 = func3;
			this.rs1 = rs1;
			this.rs2 = rs2;
			this.func7 = func7;
			this.amo_func = amo_func;
			rm = func3;
		}

		public static R parse(int data) {
			return new R(
					(data >> 7) & mask(5),
					(data >> 12) & mask(3),
					(data >> 15) & mask(5),
					(data >> 20) & mask(5),
					(data >> 25) & mask(7),
					(data >> 27) & mask(5)
			);
		}
	}
	
	public static class B {
		int func3;
		int rs1;
		int rs2;
		int imm;
		public B(int func3, int rs1, int rs2, int imm) {
			this.func3 = func3;
			this.rs1 = rs1;
			this.rs2 = rs2;
			this.imm = imm;
		}

		public static B parse(int data) {
			int imm = data & (1 << 31);
			imm |= ((data >>  7) & 1) << 30;
			imm |= ((data >> 25) & mask(6)) << 24;
			imm |= ((data >>  8) & mask(4)) << 20;
			imm >>= 19;
			
			return new B(
					(data >> 12) & mask(3),
					(data >> 15) & mask(5),
					(data >> 20) & mask(5),
					imm
			);
		}
	}
	
	public static class S {
		int func3;
		int rs1;
		int rs2;
		int imm;
		public S(int func3, int rs1, int rs2, int imm) {
			this.func3 = func3;
			this.rs1 = rs1;
			this.rs2 = rs2;
			this.imm = imm;
		}

		public static S parse(int data) {
			int imm = data & (mask(7) << 25);
			imm |= ((data >> 7) & mask(5)) << 20;
			imm >>= 20;
			
			return new S(
					(data >>> 12) & mask(3),
					(data >>> 15) & mask(5),
					(data >>> 20) & mask(5),
					imm
			);
		}
	}
}
